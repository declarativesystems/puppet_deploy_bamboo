# Puppet Deploy Plugin for Bamboo Server

## What's this?
A plugin to trigger a refresh of your [Puppet Control Repository](https://docs.puppet.com/pe/latest/cmgmt_control_repo.html#setting-up-a-control-repository)
on your Puppet Enterprise Master as part of a Plan.

## Features
* Simple configuration though the Bamboo Server web interface
* Ability to test and diagnose deployment problems
* Optional (and highly encouraged) SSL certificate validation
* Confirmation of queued update in the Bamboo build log

## Setup
* Install the plugin (follow the Atlassian Marketplace instructions)

### Getting ready to add Puppet Deploy to a new plan
1. Choose `Create a new plan` from the `Create` menu, or edit an existing plan
![new plan](images/new_plan.png)
2. Complete the setup of the new plan by filling out the form.  Towards the bottom of the screen is a drop-down where you can choose to link to a new repository.  Select the appropriate git repository connection from the list and fill in the connection details, then click `Create` 
![plan setup](images/plan_setup.png)

### Getting ready to add Puppet Deploy to an existing plan
1. From the `Build` -> `All build plans` screen, select the plan you would like to add Puppet Deploy to
![build dashboard](images/build_dashboard.png)
2. Click `Actions` -> `Configure plan`
![configure plan](images/configure_plan.png)
3. Navigate to the `Tasks` tab
![tasks](images/tasks.png)


### Adding the Puppet Deploy task
1. Click `Add task` and select `Puppet Deploy` from the list of tasks.  You can filter the list using the search box or by clicking `Deployment` under the `Task types` heading
    ![tasks](images/add_task.png)
2. Immediately after adding the task, Bamboo will load a page to configure the plugin, enter the following details:
    ![setup](images/setup.png)
    1. Fully Qualified Domain Name of the puppet master
    2. RBAC authentication token (can be created [manually](https://docs.puppet.com/pe/latest/rbac_token_auth.html) or with [pe_rbac](https://github.com/declarativesystems/pe_rbac))
    3. Optional but highly recommended:  Copy and paste the contents of `/etc/puppetlabs/puppet/ssl/ca/ca_crt.pem` into the textarea to allow SSL certificate validation
    4. Verify that communication between Bamboo and Puppet Enterprise is working:
        * Leave the `Environment` textbox blank to test communication **only**
        * If you would like to try out deploying a particular branch, type its name in the `Environment` textbox, eg `production`
        * If you **always** want to deploy this particular branch, tick the `Always deploy the selected environment` checkbox, otherwise Puppet Deploy will attempt to detect the what branch Bamboo is currently testing
        * Click the `Deploy` button to attempt deployment immediately.  The result  will be displayed below the button
    5. Click `Save` to store your choices and complete the Puppet Deploy setup process
    6. Bamboo will now return you to the `Configure Tasks` screen.  Tick `Enable this plan` and then click `Create`
    7. Bamboo setup is now complete, Puppet Deploy will attempt to deploy to Puppet Enterprise whenever Bamboo detects changes in the git repository and processes the `Puppet Deploy` task
   
### Notes:
* You can click `Deploy` on the settings page any time to trigger a refresh of environment corresponding to the selected branch 
* The `Environment` field is only used with the `Deploy` button, _unless you tick `Always deploy the selected environment`, in which case it will be used for every successful build_
* The Puppet Deploy plugin does not carry out any testing (...yet?)

## Networking requirements
* Bamboo server(s) must be able to connect to the Puppet Master on TCP port 8170
* Web browser must be able to connect to servlets running within Bamboo Server

## How it works

### Manual Deployment (`Deploy` button)
1. Browser makes a request to Bamboo server using the information in the dialogue box
2. Bamboo server makes a new and separate request to Puppet Enterprise, capturing the response
3. Response sent back to browser and reported back to user.  If there was an error communicating with either Bamboo or Puppet, then this is also captured and reported.

### Automatic Deployment (`git push`)
1. User runs `git push` from workstation
2. Bamboo detects new git commits
3. Bamboo executes each task in the Plan until it reaches the `Puppet Deploy` task (note that failing tasks can prevent this happening)
3. The plugin looks up the saved settings for the Task and requests a refresh of the environment that has the same name as the branch that was just pushed
4. The result of the deploy operation is recorded in the Bamboo job history

## Diagnostics
The easiest way to test if deployments are working is to use the `Deploy` button on the Puppet Deploy Task settings page.  Some examples are given below:

### Successful deployment
![success](images/deploy_ok.png)

### Failed deployment (Due to incorrect authentication token)
![fail](images/deploy_fail.png)

## Build logs
Each time Bamboo runs a plan it logs all output in the build log, so for debugging past builds the logs should be the first place to look:

### Successful deployment
![success](images/log_ok.png)

### Failed deployment (Due to incorrect authentication token)
![fail](images/log_fail.png) 
 

## Troubleshooting

### Builds not triggered
* Check the polling interval (`Actions` -> `Configure plan` -> `Triggers` -> `Repository polling` ) 
* Check that no Task executed before Puppet Deploy are failing, breaking the pipeline
* Check that the plan is enabled

### Error detecting updated branch
* For some repositories, it may not be possible to detect the branch being updated, in this case tick choose an environment to always be deployed and tick `Always deploy the selected environment` on the Puppet Deploy settings page (local file-based git repositories seem definitely to be affected by this). 

### Connectivity
Connectivity between Bamboo and Puppet can be tested from the Bamboo server using `curl`:

```shell
curl -k https://PUPPET.MASTER.FQDN:8170
```

Where `PUPPET.MASTER.FQDN` is the Puppet Master's fully qualified hostname.

If the above command fails, test to see if the port open at the source by logging into the Puppet Master and running the same command.  If data is now returned, then the problem lies in the network.  If errors are still reported, check the status of the local firewall (`iptables`) and that the Puppet Master is configured correctly.

While the plugin doesnt use `curl` internally, the command is perfect for testing overall connectivity.

#### Proxy Servers
If you are using a proxy server, its likely that you will have problems using this plugin.

If you would like assistance, please open a new issue with a description of:

* The problem you are having
* Your proxy server and network
* Whether the proxy server must/must not be used to access the Puppet Master REST Web Services
* As much information about the plugin setup as possible - version, screenshots, etc.

### Puppet Deployment
The job of the plugin comes to an end when Puppet Code Manager reports status `queued` to prevent a long pause when doing `git push`.

At this point, no deployment has yet taken place.  Puppet Enterprise will now carry out the following steps, any or all of which could fail:

1. Download the requested branch of the control repository (from git) to the [staging area](https://docs.puppet.com/pe/latest/code_mgr.html#understanding-file-sync-and-the-staging-directory)
2. Download each item in the [`Puppetfile`](https://github.com/puppetlabs/r10k/blob/master/doc/puppetfile.mkd) (from Puppet Forge/git)
3. Update the live copy of the code (including anything present on compile-masters)

To diagnose the above failures, the logs on the Puppet Master must be inspected:
 
```shell
tail -f /var/log/puppetlabs/puppetserver/*
```


### General bugs
Most errors encountered by the plugin will be written to the Bamboo job's log output, therefore the best place to look for errors is the Bamboo GUI itself.

If you combine the information above with the corresponding logs on from the Puppet Master, you should have everything you need to be able to diagnose and resolve deployment errors on-push.

Note:  Don't forget the `Deploy` button in the GUI too... It should be able to detect the vast majority of deployment errors.
