# 2.0.0

## Features
* Support for onceover
* Support waiting for deployment to complete so that final deployment status can
  be reported
* Support for skipping deployment for named environments
* Check that puppet's revision ID matches the build
* UI improvements


## Bugs
* Fixed error where chrome wrongly clobbers `Puppet Master FQDN` and 
  `RBAC Token` with the bamboo username and and password
* Fixed handling of pushing several branches at once
* Remote/non-default agents are now working