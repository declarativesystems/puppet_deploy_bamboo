---
title: Puppet Enterprise
taxonomy:
    category: docs
docdir: /products/puppet_deploy_bamboo
---
Puppet Enterprise needs to be configured for Code Manager deployments before 
the Bamboo task can do any deployments: 

1. [Configure Code Manager](https://puppet.com/docs/pe/2019.1/code_mgr_config.html#configuring-code-manager)
   to connect to the your Puppet Control Repository
    * This is sometimes done during Puppet Enterprise Installation
2. Generate a token using:
    * [Manual instructions](https://puppet.com/docs/pe/2019.1/rbac_token_auth_intro.html)
    * [pe_rbac](https://github.com/declarativesystems/pe_rbac)
    * The token can be found at `~/.puppetlabs/token`
3. Obtain the CA Certificate (optional)
    * Copy the contents of `/etc/puppetlabs/puppet/ssl/ca/ca_crt.pem` from the
      Puppet Master