---
title: Puppet Deploy task
taxonomy:
    category: docs
docdir: /products/puppet_deploy_bamboo
---

! You must setup [Puppet Enterprise for code deployment](puppet_enterprise)
! before code can be deployed by Bamboo

1. Navigate to your plan and click `Actions` -> `Configure plan`, then edit 
   `Default job` (or create your own).
2. Click `Add task`
   
   ![Add task](../../images/add_task.png)
3. Find `Puppet Deploy` under the `Deployment` category and select it 

   ![Add task](../../images/add_task_puppet_deploy.png)
4. Configure the Puppet Deploy task using its configuration page with the 
   details you collected from Puppet Enterprise:

   ![setup](../../images/puppet_deploy_task_setup.png)
    1. `Puppet Master FQDN`
    2. `RBAC Token`
    3. `CA Certificate` (optional)
    4. `Skip Environments` (optional)
        * Space delimited list of environments that will not be deployed
          automatically by the Bamboo Task eg `production`
    5. `Wait`
        * _checked_ - Wait for Puppet Code Manager to return deployment status during 
          `git push`
        * _unchecked_ - Stop once Puppet accepts deployment request
6. Click `Deploy` to do a test deployment
7. Click `Save` to save changes and enable the Task
