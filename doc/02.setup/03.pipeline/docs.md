---
title: Pipeline
taxonomy:
    category: docs
docdir: /products/puppet_deploy_bamboo
---

To make a test and deployment pipeline for Puppet Enterprise, your Job should 
contain:

1. Source Code Checkout
2. Onceover
3. Puppet Deploy

![pipeline setup](../../images/pipeline_setup.png)

This way, the pipeline will only deploy code to Puppet if `onceover` tests are
passing.

! If you have multiple, **separate** Puppet Masters, you can deploy code to each
! of them by adding more Puppet Deploy tasks

! The choice of tasks in the pipeline is up to you - you can choose to skip 
! onceover or deployment if you like