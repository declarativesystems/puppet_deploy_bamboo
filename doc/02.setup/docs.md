---
title: Setup
taxonomy:
    category: docs
docdir: /products/puppet_deploy_bamboo
---

! Install the plugin following the 
! [Atlassian Marketplace instructions](https://marketplace.atlassian.com/apps/1217912/puppet-deploy-for-bamboo-server?hosting=server&tab=installation)

After installation you will have two new Bamboo tasks available which are
configured independently per-job:

* [Onceover](onceover)
* [Puppet Deploy](puppet_deploy)

The next steps are to:
* Combine the tasks into a [test and deployment pipeline](pipeline)
* Configure [plan branches](branches) for each Puppet Environment