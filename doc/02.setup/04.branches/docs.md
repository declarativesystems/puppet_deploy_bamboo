---
title: Branches
taxonomy:
    category: docs
docdir: /products/puppet_deploy_bamboo
---

Bamboo can be configured to create and delete 
[plan branches](https://confluence.atlassian.com/bamboo/using-plan-branches-289276872.html)
as the Puppet Control Repository changes. This ensures that you test the 
branches you want to test and don't accumulate stale plans.

This is configued by selecting `Configure plan` -> `Branches` from your plan's
`Actions` menu.

![plan branches](../../images/plan_branch.png)

## Existing branches
The `Create plan branch` and `Delete plan branch` settings only relate to new
branch activity on the git repository. 

It's best to have a plan branch for every Puppet environment. To setup existing
branches:

1. Click `Create plan branch`

   ![create plan branch](../../images/create_plan_branch_1.png)
2. From the `Branch name` control, pick each of the branches you wish to build, 
   then click `Create`
   
   ![create plan branch](../../images/create_plan_branch_2.png)

! Plan branches will be built (deployed) immediately when `Save` is clicked if
! `Enable plan` is ticked

! Its easy to run the pipeline for a single Puppet Environment, just select the
! plan branch and click `Run branch` from the `Run` menu

![single puppet environment](../../images/pipeline_puppet_environment.png)
