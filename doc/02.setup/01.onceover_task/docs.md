---
title: Onceover task
taxonomy:
    category: docs
docdir: /products/puppet_deploy_bamboo
---
The _Onceover task_ runs `onceover` in your Puppet Control Repository and
formats the output.

! You must setup your [Puppet Control Repository](puppet_control_repository) and 
! [Bamboo agent](bamboo_agent) before tests can be run

1. Navigate to your plan and click `Actions` -> `Configure plan`, then edit 
   `Default job` (or create your own).
2. Click `Add task`
   
   ![Add task](../../images/add_task.png)
3. Find `Onceover` under the `Tests` category and select it 

   ![Add task](../../images/add_task_onceover.png)
4. Configure the Onceover task using its configuration page:

   ![configure onceover](../../images/onceover_task_setup.png)
   
   The defaults should work in most cases, but can be customised:
    * `Use bundler`
        * Run `bundle install` first
        * If not using a `Makefile`, prefix all `onceover` commands with 
          `bundle exec`
    * `Add code quality tests`
        * If using `Autodetect` **and** `Makefile` already runs 
          `onceover-codequalty`, do nothing
        * Otherwise run `onceover run codequality`
    * `How to run onceover`
        * `Autodetect`
            * If `Makefile` found, run default target (`make`)
            * Otherwise run `onceover run spec`
        * `onceover run spec`
            * Run this command to invoke `onceover`
5. Click `Save` to add the configured task to the job.