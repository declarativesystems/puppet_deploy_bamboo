---
title: Puppet Control Repository
taxonomy:
    category: docs
docdir: /products/puppet_deploy_bamboo
---

`onceover` must be configured within your 
[Puppet Control repository](https://puppet.com/docs/pe/2019.1/control_repo.html#setting-up-a-control-repository) 
before tests can be run. If you have a suitably configured workstation, you can 
run `onceover` locally to get instant feedback as you set it up.

Instructions to setup a workstation are identical to the 
[Bamboo agent](../bamboo_agent#bamboo-agent) instructions.

!! `onceover` works with Puppet Control Repositories, **NOT** regular 
!! Puppet Modules

## Control Repository Setup
We recommend following the official 
[`onceover` instructions](https://github.com/dylanratcliffe/onceover) to get
started. 

You should use a [`Gemfile`](https://bundler.io/gemfile.html) to control the
version of `onceover` used for testing. The Bamboo task supports `bundler` 
natively so this is an easy way to change the `onceover` version Bamboo uses 
without runningthe `gem` command on the Bamboo agents.

! For repeatable builds, version your RubyGems in the `Gemfile`, like this: 
! ```
! gem 'onceover', '3.13.4'
! ```

!! Onceover 3.13.x is required for test parsing

You may find it useful to setup `onceover` for additional tests such as 
[onceover-codequality](https://github.com/declarativesystems/onceover-codequality)
Instructions for this are on the 
[Declarative Systems Blog](http://declarativesystems.com/blog/onceover#enhancing-onceover)

## Example puppet control repositories
* [Official Puppet control repository (no `onceover`)](https://github.com/puppetlabs/control-repo)
  
  This is a mostly empty control repository that is often used as a base for
  organsiations deploying Puppet Enterprise.
* [Control repository configured (with `onceover`)](https://github.com/declarativesystems/puppet-control-test)
 
  Basic control repository configured with `onceover` and 
  `onceover-codequality`, used for testing this plugin
