---
title: Bamboo Agent
taxonomy:
    category: docs
docdir: /products/puppet_deploy_bamboo
---

!!! Bamboo agents on Windows are not supported

Since `onceover` is an external [Ruby](https://www.ruby-lang.org) program, the 
Bamboo agent running it must be configured to run Ruby and also needs 
development libraries and access to either The Internet or a 
[local gem repository](https://jfrog.com/artifactory/) to install the 
[RubyGems](https://en.wikipedia.org/wiki/RubyGems) (libraries) that `onceover`
needs.

## Docker
Docker images are a convenient way to get up and running in Bamboo quickly.

`onceover`'s gem dependencies require several development libraries and tools,
notably [`cmake`](https://en.wikipedia.org/wiki/CMake).  We have created the
Docker image 
[declarativesystems/ruby_dev](https://hub.docker.com/r/declarativesystems/ruby_dev)
which already has them installed.

To configure `onceover` to run in Docker:

1. Navigate to your plan and edit the job containing the `onceover` task
2. Navigate to the `Docker` tab
3. On the `Isolate build` section, click `Docker container`
4. Type the name and version of the docker image you want to use

   ![docker setup](../../../images/docker_setup.png)

If you have free and direct internet access, you can use the provided Docker
image directly and it will download the RubyGems that `onceover` needs when
tests run, otherwise your free to use your own image.

!!! Docker on windows is not supported [by Bamboo](https://jira.atlassian.com/browse/BAM-20219)

## Bamboo agent

Regular Bamboo agents should be configured with support for:
* Ruby - version 2.4 or 2.5 is recommended, the easiest way to install is 
  usually one of: 
    * [rbenv](https://github.com/rbenv/rbenv) 
    * [RVM](https://rvm.io/)
* Development tools
    * Redhat: `yum group install "Development Tools"`
    * Debian: `apt-get install build-essential` 
* `bundler` 2.x gem (optional) 
    * `gem install bundler`
* `Python` + `pyyaml` (optional - for better `YAML` validation)
    1. Install the `python-pip` package (from EPEL in RedHat systems)
    2. `pip install pyyaml`
* `cmake`
* `git`
* If not using `bundler`, any RubyGems needed must be installed manually:
    * `gem install onceover` (mandatory)
    * `gem install onceover-codequalty` (required to run code quality tests)
    * You are strongly recommended to install `bundler` and manage versions with
      the `Gemfile` instead of manually like this

### Bamboo agent capabilities
To make sure `onceover` is only run on Bamboo agents that support it, use
_Agent-specific capabilities_:

1. Identify a `onceover` capable agent and click `Add capability`
   
   ![Add capability 1](../../../images/agent_capability_1.png)
2. Add a custom capability. The name and value are simple strings so use
   whatever you like.
   
   ![Add capability 2](../../../images/agent_capability_2.png)
3. Repeat for all capable agents
4. Add your custom capability as a job requirement 
   
   ![Add job requirement](../../../images/agent_capability_3.png)


## Docker Tips

You can speed-up the docker image by pre-installing the RubyGems for your
version of `onceover`. If your using `bundler` you can extract the `RUN` command
to add to `Dockerfile` by running: 

```shell
bundle list | sed 's/(/--version /g' | tr -d ')' | sed 's/*/gem install/g' | sed 's/$/ \&\& \\/' 
```

From the top-level of your puppet control repository.

