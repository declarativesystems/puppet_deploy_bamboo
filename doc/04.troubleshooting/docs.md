---
title: Troubleshooting
taxonomy:
    category: docs
docdir: /products/puppet_deploy_bamboo
---

## Wrong branch tested/deployed
If `branch` is set at the plan level, it overrides that set at the plan branch
level. Make sure the `branch` is not set in the repository settings:

![do not set branch](../images/plan_repo_config.png)

## Branche names with hyphens aren't deployed
Puppet will refuse to deploy any branches with hyphens in the name which is an 
issue if you use the `create branch` functionality to fix Jira tickets. The 
closest workaround in this case is to replace hyphens with underscores, eg 
`PUP-1234` becomes `PUP_1234`. 

Unfortunately this breaks automatic tracking of branches to JIRA issues. 



## Connectivity
The plugin only ever talks to the Puppet Master from the Bamboo agent so
these server(s) must be able to connect to the Puppet Master on TCP port 
8170.

Reported connectivity problems can be investigated by testing from the Bamboo
agent using `curl`:

```shell
curl -k https://PUPPET.MASTER.FQDN:8170
```

_`PUPPET.MASTER.FQDN` is the Puppet Master's fully qualified hostname_


If this command fails, check if the port is open at source by logging into the
Puppet Master and running the same command. If data is now returned, then the 
problem lies in the network.

If errors are still reported, check the status of the local firewall 
(`firewalld`/`iptables`) and that the Puppet Master is configured correctly.

While the plugin doesnt use `curl` internally, the command is perfect for 
testing overall connectivity.

## Puppet Deployment errors
The JSON output from the Puppet Code Manager service is captured by the plugin
and should be enough to diagnose most deployment failures. 

Top errors causes:
* Bad `Puppetfile` syntax
* Referencing a module/version of module/git tag that doesn't exist

## Proxy Servers
The plugin does not support Proxy Servers at the moment.

Please 
[suggest a new feature](https://declarativesystems.atlassian.net/servicedesk/customer/portals)
if you would like proxy support in a future version and include details of:

* Errors encountered (if you already tried the plugin)
* Your proxy server and network
* Whether the error was caused by talking to Puppet using the proxy when we
  should not have, or whether we need to use a proxy server to reach Puppet


## Plugin errors
If you get errors that have come form the Plugin itself rather then Puppet
Enterprise, please log a 
[support request](https://declarativesystems.atlassian.net/servicedesk/customer/portals)
including:
* Summary of the error
* What should happen
* What really happened
* Steps to reproduce/what you were doing when the error happened
* How often the error occurs (sometimes/always/etc)
* Any screenshots/logs demonstrating the error
* Bamboo server/git client versions
* Puppet Enterprise version
* Onceover version
* OS used
* Browser version
