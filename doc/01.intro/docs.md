---
title: Overview
taxonomy:
    category: docs
docdir: /products/puppet_deploy_bamboo
---
## Overview
Build a complete Puppet Enterprise testing and deployment pipeline in Bamboo
Server.

![pipeline ok](../images/onceover_pass_tests_1.png)

The plugin provides two independent Bamboo tasks that you can combine as 
required:

### Onceover

> The gateway drug to automated infrastructure testing with Puppet

[`onceover`](https://github.com/dylanratcliffe/onceover/) is created and 
maintained by Dylan Ratcliffe from Puppet and is a tool to let you quickly and 
easily test your Puppet Control Repository before you do any real testing on 
your dedicated Puppet testing nodes.

The Bamboo task is able to parse `onceover` output to report on which tests are
passing or failing.

! There is some setup work to do in order to make this work.

### Puppet Deploy

Deploy code to a Puppet Enterprise Master and check that the process completed.
