---
title: Onceover tests
taxonomy:
    category: docs
docdir: /products/puppet_deploy_bamboo
---

The Onceover task parses `onceover` and `onceover-codequality` output to figure
out:
 
**Passing tests**

![passing tests - detail](../../images/onceover_pass_tests_2.png)

**Failing tests** 

![passing tests - summary](../../images/onceover_fix_1.png)

You can use the cog button to the right of a failed test to open a Jira ticket
and track the problem if you like:

![add to jira](../../images/onceover_jira.png)

!! Quarantine is not currently functional
 
## Bamboo test tracking
Bamboo will track the progress of failing tests in a given plan for you. 

Initially, a failed test will be marked **New**:

![new failure](../../images/onceover_fail_new.png)

If the same test fails in the future, it will be marked **Existing failures**:

![passing tests - summary](../../images/onceover_fix_2.png)

Once a test is fixed, it will be marked **Fixed** for one build:

![passing tests - summary](../../images/onceover_fix_3.png)

## Debugging test failures
Failed tests contain the error message from `onceover`. If you would like to see
the message in context, click the `Logs` tab to examine the build log:

![failing tests - build log](../../images/onceover_fail_log.png)

! If your able to run `onceover` on your workstation you should be able to 
! replicate and fix the failing test without having to iterate through `git` and
! Bamboo builds

!! Parsing test output requires `onceover` results match known patterns so
!! unexpected output could break the parser. We recommend versioning your
!! RubyGems to guard against unintended upgrades.  