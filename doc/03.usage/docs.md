---
title: Usage
taxonomy:
    category: docs
docdir: /products/puppet_deploy_bamboo
---

Most sites will configure the bamboo plan to:
* Create/delete plan branches as the repository changes
* Test and deploy when changes are detected in each branch

This means Puppet developers won't normally need to do anything beyond push
their changes to run the plan.

One exception to this might be the `production` branch. Some sites might choose
to make deploying this branch a manual process, triggered by either clicking the
`Deploy` button in the plugin or by running the `puppet code deploy` command on
the Puppet Master.

! Anyone with access to the plugin settings screen can request deployment of 
! _any_ Puppet environment (branch). If you need to restrict access to 
! particular branches, use 
! [Puppet RBAC permissions](https://puppet.com/docs/pe/2019.1/rbac_permissions_intro.html#user-permissions)
! to limit the reach of the RBAC token.


## Overall plan status
It's easy to see overall status from the bar at the top of the build results. 
Ideally everything will pass and you will get a green bar:

![plan status ok](../images/plan_pass_summary.png)

If errors are encountered, look at the message and see the notes for fixing:
* [Onceover Tests](onceover_tests)
* [Puppet deployments](puppet_deploy_status)

### Deploy code whenever you like
You can return to the Puppet Deploy task settings page any time you like by 
clicking `Actions` -> `Edit plan` and then editing the job that contains the
Puppet Deploy task.

From the settings screen, use the radio buttons under `Actions` to deploy
either _All environments_ or a _Specific environment_ and then click `Deploy`.
 
When deploying a specific environment, type the name of the environment to 
deploy in the text box. 

The plugin displays the results from Puppet:

![deploy branch](../images/deploy_branch_ok.png)