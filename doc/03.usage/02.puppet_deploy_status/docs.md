---
title: Puppet deployment status
taxonomy:
    category: docs
docdir: /products/puppet_deploy_bamboo
---

If you need to investigate the status of code deployment as reported by Puppet
Enterprise, click through to the `Logs` tab of the build you are interested in.

Passing builds will report the `git` commit and branch deployed by Puppet:

![deploy ok](../../images/deploy_pass_detail.png)

Failing builds will contain the error message received from Puppet as part of 
the build log:

![deploy failed](../../images/deploy_fail_detail.png)

## Investigating deployment errors
Puppet Code Manager can fail mid-deployment for reasons such as: 

* Errors in `Puppetfile`
* Trouble reaching Puppet Forge
* ...etc

These errors are normally captured inside the JSON output that's returned on
error.

If a branch reports `MISMATCH`, this means a different `git` commit was deployed
by Puppet vs the revision that was tested by Bamboo. The most likely cause of
this would be connecting Puppet to a completely different git server/repository.

The Puppet Deployment task settings page makes it easy to debug failures
independently of a build:

![debug deployment errors](../../images/deploy_error_debug.png)

In this case, we can see that Puppet failed deployment of one environment due to
bad syntax in its `Puppetfile`.