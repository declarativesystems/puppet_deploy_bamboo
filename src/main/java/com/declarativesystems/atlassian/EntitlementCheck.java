package com.declarativesystems.atlassian;

import java.util.HashMap;

public interface EntitlementCheck {
    String LICENSE_MESSAGE_KEY = "com.declarativesystems.bamboo.puppetdeploy.LICENSE_STATUS";
    String UNLICENSED           = "unlicensed";
    String INVALID              = "invalid";
    String LICENSED             = "licensed";
    HashMap<String,String> LICENSE_MESSAGE = new HashMap<String,String>(){{
        put(UNLICENSED, "Please purchase a Puppet Deploy Plugin for Bamboo Server license.");
        put(INVALID,    "Invalid license: Your evaluation license of Puppet Deploy Plugin for Bamboo Server expired. Please purchase a new license.");
        put(LICENSED,   null);
    }};

    /**
     * Get the text to display to user if we are not licensed
     * @return Text to display to user or null if we are licensed properly
     */
    String getLicenseMessage();
}
