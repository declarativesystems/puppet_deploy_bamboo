package com.declarativesystems.atlassian;

import com.atlassian.plugin.spring.scanner.annotation.component.BambooComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.upm.api.license.entity.PluginLicense;

import javax.inject.Inject;


@BambooComponent
public class EntitlementCheckImpl implements EntitlementCheck {

    private PluginLicenseManager pluginLicenseManager;

    @Inject
    public EntitlementCheckImpl(@BambooImport PluginLicenseManager pluginLicenseManager)
    {
        this.pluginLicenseManager = pluginLicenseManager;
    }

    @Override
    public String getLicenseMessage() {
        String status;
        if (pluginLicenseManager.getLicense().isDefined()) {
            PluginLicense license = pluginLicenseManager.getLicense().get();
            if (license.getError().isDefined()) {
                // handle license error scenario
                // (e.g., expiration or user mismatch)
                status = EntitlementCheck.INVALID;
             } else {
                // handle valid license scenario
                status = EntitlementCheck.LICENSED;
            }
        } else {
            // handle unlicensed scenario }
            status = UNLICENSED;
        }

        return status;
    }

}
