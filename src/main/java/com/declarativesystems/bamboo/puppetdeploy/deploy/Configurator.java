/*
 * Copyright 2017 Declarative Systems PTY LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.declarativesystems.bamboo.puppetdeploy.deploy;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.TaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.declarativesystems.atlassian.EntitlementCheck;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;

public class Configurator implements TaskConfigurator
{
    private EntitlementCheck entitlementCheck;
    static final String PUPPET_MASTER_FQDN = "puppetMasterFqdn";
    static final String TOKEN = "token";
    static final String CA_CERT = "caCert";
    static final String ENVIRONMENT = "environment";
    static final String ENVIRONMENTS = "environments";
    static final String DEPLOY_ENVIRONMENT = "deployEnvironment";
    static final String ENVIRONMENTS_ALL = "all";
    static final String SKIP_ENVIRONMENTS = "skipEnvironments";

    @Inject
    public Configurator(EntitlementCheck entitlementCheck) {
        this.entitlementCheck = entitlementCheck;
    }

    private static final Map<String,String> environments = new HashMap<String,String>() {{
        put(ENVIRONMENTS_ALL, "All environments");
        put("specific", "Specific environment:");
    }};


    @Override
    public void populateContextForEdit(@Nonnull Map<String, Object> map, @Nonnull TaskDefinition taskDefinition)
    {
        map.put(PUPPET_MASTER_FQDN, taskDefinition.getConfiguration().get(PUPPET_MASTER_FQDN));
        map.put(TOKEN, taskDefinition.getConfiguration().get(TOKEN));
        map.put(CA_CERT, taskDefinition.getConfiguration().get(CA_CERT));
        map.put(SKIP_ENVIRONMENTS, taskDefinition.getConfiguration().get(SKIP_ENVIRONMENTS));

        // select the all environments as the default action
        map.put(DEPLOY_ENVIRONMENT, ENVIRONMENTS_ALL);
        map.put(ENVIRONMENTS, environments);

        map.put(EntitlementCheck.LICENSE_MESSAGE_KEY,
                EntitlementCheck.LICENSE_MESSAGE.get(
                        entitlementCheck.getLicenseMessage()
                )
        );
    }

    @Override
    public void populateContextForCreate(@Nonnull Map<String, Object> map)
    {
        // select the all environments as the default action
        map.put(DEPLOY_ENVIRONMENT, ENVIRONMENTS_ALL);
        map.put(ENVIRONMENTS, environments);

        map.put(EntitlementCheck.LICENSE_MESSAGE_KEY,
                EntitlementCheck.LICENSE_MESSAGE.get(
                        entitlementCheck.getLicenseMessage()
                )
        );
    }

    @Override
    public void validate(@Nonnull ActionParametersMap settings, @Nonnull ErrorCollection errorCollection)
    {
        Map<String, List<String>> errors = new HashMap<>();

        // FQDN - must be a reachable host
        String puppetMasterFqdn = settings.getString(PUPPET_MASTER_FQDN, "");
        if (StringUtils.isBlank(puppetMasterFqdn))
        {
            errors.put(PUPPET_MASTER_FQDN, new ArrayList<>(Collections.singletonList("Puppet Master FQDN field is blank, please supply one")));
        }
        else
        {
            // validating hostnames/IP addresses is HARD these days, so just attempt to lookup
            // an IP address and report failure if encountered
            try
            {
                InetAddress.getByName(puppetMasterFqdn);
            }
            catch (UnknownHostException e)
            {
                errors.put(PUPPET_MASTER_FQDN, new ArrayList<>(Collections.singletonList("Unable to resolve Puppet Master FQDN to an IP address:  " + e.getMessage())));
            }
        }


        // TOKEN - must be set
        if (StringUtils.isBlank(settings.getString(TOKEN, "")))
        {
            errors.put(TOKEN, new ArrayList<>(Collections.singletonList("Token field is blank, please supply one")));
        }

        errorCollection.addFieldErrors(errors);
    }

    /**
     * Save :)
     * @param actionParametersMap incoming parameters from form
     * @param taskDefinition no idea...
     * @return Map of settings to use for automatic form population
     */
    @Nonnull
    @Override
    public Map<String, String> generateTaskConfigMap(@Nonnull ActionParametersMap actionParametersMap, @Nullable TaskDefinition taskDefinition)
    {
       // final Map<String, String>= super.generateTaskConfigMap(params, previousTaskDefinition);
        Map<String, String> config = new HashMap<>();
        config.put(PUPPET_MASTER_FQDN, actionParametersMap.getString(PUPPET_MASTER_FQDN));
        config.put(TOKEN, actionParametersMap.getString(TOKEN));
        config.put(CA_CERT, actionParametersMap.getString(CA_CERT));
        config.put(SKIP_ENVIRONMENTS, actionParametersMap.getString(SKIP_ENVIRONMENTS));

        return config;
    }
}
