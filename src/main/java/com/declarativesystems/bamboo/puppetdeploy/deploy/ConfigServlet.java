/*
 * Copyright 2017 Declarative Systems PTY LTD
 * Copyright 2016 Puppet Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.declarativesystems.bamboo.puppetdeploy.deploy;

import com.atlassian.plugin.spring.scanner.annotation.component.BambooComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.declarativesystems.pejava.codemanager.Deploy;
import com.declarativesystems.pejava.codemanager.DeployResult;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;

@BambooComponent
public class ConfigServlet extends HttpServlet {
    private final UserManager userManager;
    public final static String STATUS_FIELD = "status";
    public final static String REPLY_FIELD = "reply";
    public final static String OUTPUT_FIELD = "htmlTableRows";
    public final static String REQUEST_FIELD = "requestId";
    public final static String DEBUG_FIELD = "debug";
    public final static String ENVIRONMENT_FIELD = "environment";

    private final Deploy deploy;

    /**
     * Request was processed end-to-end OK
     */
    public final static String STATUS_OK = "ok";

    /**
     * Request was sent OK but puppet gave an error
     */
    public final static String STATUS_PUPPET_ERROR = "puppet_error";

    /**
     * Something was wrong with the request, eg host unreachable
     */
    public final static String STATUS_REQUEST_ERROR = "request_error";

    /**
     * Something went wrong inside this servlet, eg bad algorithm
     */
    public final static String STATUS_INTERNAL_ERROR = "internal_error";

    /**
     * User logged out or insufficient permission
     */
    public final static String STATUS_PERMISSION_ERROR = "permission_error";

    @Inject
    public ConfigServlet(
            @BambooImport final UserManager userManager,
            final Deploy deploy) {
        this.userManager = userManager;
        this.deploy = deploy;
    }


    private boolean permission_denied() {
        UserKey userKey = userManager.getRemoteUserKey();
        return userKey == null || !userManager.isAdmin(userKey);
    }


    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        // getting debug logs is too hard so build up a narrative of what went on
        // and include it in the JSON we return
        StringBuilder debug = new StringBuilder();
        debug.append("servicing request\n");

        Map<String, Object> result = new HashMap<>();

        if (permission_denied()) {
            result.put(REPLY_FIELD, "Insufficient permission or not logged in");
            result.put(STATUS_FIELD, STATUS_PERMISSION_ERROR);
        } else {
            // where's my input stream??? https://answers.atlassian.com/questions/32117784/missing-post-data-in-confluence-5.7-servlet-plugin
            // argh!!!!
            // for some bizare reason, my POST data ends up as the NAME of the first parameter (not the value)...
            // why is everything so hard...
            String jsonDataStoredInParameterName = req.getParameterNames().nextElement();
            String requestId = null;
            boolean proceed = true;

            Map<String, String> data = null;
            try {
                // :)  https://stackoverflow.com/questions/31819398/why-am-i-getting-an-unchecked-assignment-warning
                data = new Gson().fromJson(jsonDataStoredInParameterName,
                        new TypeToken<Map<String, String>>() {
                        }.getType());
                debug.append("finished parsing request JSON\n");
                if (data == null) {
                    proceed = false;
                    result.put(REPLY_FIELD, "no JSON received from Bitbucket server");
                    result.put(STATUS_FIELD, STATUS_INTERNAL_ERROR);
                }
            } catch (JsonSyntaxException e) {
                proceed = false;
                result.put(REPLY_FIELD, "Invalid JSON received from Bitbucket server: " + e.getMessage());
                result.put(STATUS_FIELD, STATUS_REQUEST_ERROR);
            }

            if (proceed) {
                debug.append("request is good - processing\n");
                String puppetMasterFqdn = data.get(Configurator.PUPPET_MASTER_FQDN);
                String token            = data.get(Configurator.TOKEN);
                String caCert           = data.get(Configurator.CA_CERT);
                String environment      = data.get(ENVIRONMENT_FIELD);
                requestId               = data.get(REQUEST_FIELD);

                String[] environments;

                if (StringUtils.isBlank(puppetMasterFqdn)) {
                    proceed = false;
                    result.put(REPLY_FIELD, "must supply Puppet Master FQDN");
                    result.put(STATUS_FIELD, STATUS_REQUEST_ERROR);
                } else {
                    try {
                        InetAddress.getByName(puppetMasterFqdn);
                    }
                    catch (UnknownHostException e) {
                        proceed = false;
                        result.put(REPLY_FIELD, "Unable to resolve Puppet Master FQDN to an IP address:  " + e.getMessage());
                        result.put(STATUS_FIELD, STATUS_REQUEST_ERROR);
                    }
                }

                if (proceed && StringUtils.isBlank(token)) {
                    proceed = false;
                    result.put(REPLY_FIELD, "must supply token");
                    result.put(STATUS_FIELD, STATUS_REQUEST_ERROR);
                }

                environments = StringUtils.isBlank(environment) ?
                        null: new String[]{environment};

                if (proceed && StringUtils.isBlank(requestId)) {
                    proceed = false;
                    result.put(REPLY_FIELD, "missing request ID");
                    result.put(STATUS_FIELD, STATUS_INTERNAL_ERROR);
                }


                if (proceed) {
                    try {
                        debug.append("Deploying environment(s): ")
                                .append(String.join(",", (
                                        environments == null) ?
                                        new String[] {"ALL"} :
                                        environments
                                ))
                                .append("\n");
                        String reply = deploy.deployCode(
                                puppetMasterFqdn,
                                token,
                                caCert,
                                environments,
                                true
                        );
                        debug.append("deployment returned... building response\n");
                        result.put(REPLY_FIELD, DeployResult.prettyPrintJson(reply));
                        result.put(OUTPUT_FIELD, DeployResult.toHtmlTableRows(reply));
                        result.put(
                                STATUS_FIELD,
                                DeployResult.responseStringContainError(reply) ?
                                        STATUS_PUPPET_ERROR: STATUS_OK
                        );
                        debug.append("response prepared\n");
                    } catch (IOException|CertificateException|NoSuchAlgorithmException|KeyStoreException|KeyManagementException e)
                    {
                        result.put(STATUS_FIELD, STATUS_INTERNAL_ERROR);
                        result.put(REPLY_FIELD, "Error connecting to " + puppetMasterFqdn + " - Reason: " + e.getMessage());
                    }
                }
            }

            result.put(REQUEST_FIELD, requestId);
            result.put(DEBUG_FIELD, debug.toString());
        }

        // Set standard HTTP/1.1 no-cache headers.
        resp.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");

        // Set standard HTTP/1.0 no-cache header.
        resp.setHeader("Pragma", "no-cache");

        // JSON to send back
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        out.println(new Gson().toJson(result));
    }

}

