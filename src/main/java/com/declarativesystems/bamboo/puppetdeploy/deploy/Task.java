/*
 * Copyright 2017 Declarative Systems PTY LTD
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.declarativesystems.bamboo.puppetdeploy.deploy;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.*;
import com.atlassian.bamboo.vcs.configuration.PlanRepositoryDefinition;
import com.atlassian.bamboo.vcs.configuration.VcsBranchDefinition;
import com.declarativesystems.atlassian.EntitlementCheck;
import com.declarativesystems.pejava.codemanager.Deploy;
import com.declarativesystems.pejava.codemanager.DeployResult;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Task implements TaskType {

    private final Deploy deploy;

    private final static String OUTPUT_INDENT = "\t";

    public Task(Deploy deploy) {
        this.deploy = deploy;
    }

    @Nonnull
    @Override
    public TaskResult execute(@Nonnull TaskContext taskContext) throws TaskException  {

        final BuildLogger buildLogger = taskContext.getBuildLogger();
        Map<String, String> config = taskContext.getConfigurationMap();

        String puppetMasterFqdn = config.get(Configurator.PUPPET_MASTER_FQDN);
        String token = config.get(Configurator.TOKEN);
        String caCert = config.get(Configurator.CA_CERT);
        String skipEnvironments = config.get(Configurator.SKIP_ENVIRONMENTS);

        // TaskResultBuilder initialises status field to SUCCESS so we just need to mark
        // failure conditions should they arise
        TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(taskContext);
        String[] environments;

        // licence check
        String licenseStatus = taskContext
                .getRuntimeTaskContext()
                .get(EntitlementCheck.LICENSE_MESSAGE_KEY);

        if (! (EntitlementCheck.LICENSE_MESSAGE.get(licenseStatus) == null)) {
            buildLogger.addErrorLogEntry(
                    "***" +
                    EntitlementCheck.LICENSE_MESSAGE.get(licenseStatus) +
                    " ***"
            );
        }

        try {
            // The only way to get output info on the plan page summary page seems
            // to be to write the error log (even for non-errors) - I think this is
            // needed since just seeing green isn't enough for me

            // attempt to figure out what branch was just updated
            List<PlanRepositoryDefinition> planRepositories = taskContext.getBuildContext().getVcsRepositories();

            // sure you could do this as a one-liner but in case of errors this
            // should give us a more useful stacktrace
            PlanRepositoryDefinition repositoryDefinition = planRepositories.get(0);
            VcsBranchDefinition b = repositoryDefinition.getBranch();
            String branchName = b.getVcsBranch().getName();
            String revision = taskContext.getBuildContext().getBuildResult().getCustomBuildData().get("repository.revision.number");

            Map<String,String> target = new HashMap<String,String>() {{
                put(branchName, revision);
            }};

            buildLogger.addBuildLogEntry(String.format("git branch %s revision %s: ", branchName, revision));
            environments = new String[]{branchName};

            if (Arrays.asList(skipEnvironments.split("\\s+")).contains(branchName)) {
                buildLogger.addErrorLogEntry(
                        String.format(
                                "Skipping automatic deployment of environment %s on Puppet Master %s as requested",
                                branchName,
                                puppetMasterFqdn
                        )
                );
            } else {
                String result = deploy.deployCode(puppetMasterFqdn, token, caCert, environments, true);
                boolean errors = DeployResult.responseStringContainError(result);

                // dump as a debug log
                buildLogger.addErrorLogEntry(String.format(
                        "Deployment results for Puppet Server %s:",
                        puppetMasterFqdn)
                );

                for (DeployResult deployResult : DeployResult.checkDeployResult(result, target)) {
                    if (!deployResult.isOk()) {
                        taskResultBuilder.failed();
                    }
                    buildLogger.addErrorLogEntry(OUTPUT_INDENT + deployResult.toString());
                }

                if (errors) {
                    buildLogger.addErrorLogEntry(
                            "Error reported by Puppet: \n"
                            + DeployResult.prettyPrintJson(result)
                    );
                }

            }
        } catch (NullPointerException|IndexOutOfBoundsException e) {
            taskResultBuilder = taskResultBuilder.failed();
            buildLogger.addBuildLogEntry("Unable to determine branch to deploy to Puppet");
        } catch (IOException | NoSuchAlgorithmException | KeyStoreException | KeyManagementException | CertificateException e) {
            buildLogger.addErrorLogEntry("Deployment failed:  " + e.getMessage());
            taskResultBuilder = taskResultBuilder.failed();
        }
        return taskResultBuilder.build();
    }
}
