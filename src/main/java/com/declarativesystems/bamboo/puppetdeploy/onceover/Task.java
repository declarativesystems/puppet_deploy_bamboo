package com.declarativesystems.bamboo.puppetdeploy.onceover;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.build.test.TestCollationService;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.process.ExternalProcessBuilder;
import com.atlassian.bamboo.process.ProcessService;
import com.atlassian.bamboo.task.*;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.utils.process.ExternalProcess;
import com.declarativesystems.atlassian.EntitlementCheck;
import com.declarativesystems.bamboo.puppetdeploy.ExternalProcessLogInterceptor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


public class Task implements TaskType {

    private final ProcessService processService;
    private final TestCollationService testCollationService;
    private final static String[] RUBY_VERSION = new String[] {"ruby", "--version"};
    private final static String[] BUNDLER_VERSION = new String[] {"bundle", "--version"};
    private final static String[] MANUAL_RUN_ONCEOVER = new String[] {"onceover", "run", "spec"};
    private final static String BUNDLE = "bundle";
    private final static String[] BUNDLE_INSTALL = new String[] {BUNDLE, "install"};
    private final static String[] BUNDLE_EXEC = new String[] {BUNDLE, "exec"};
    private final static String[] AUTO_RUN_ONCEOVER = new String[] {"make"};
    private final static String MAKEFILE = "Makefile";
    private final static String[] MANUAL_RUN_CODE_QUALITY = new String[] {"onceover", "run", "codequality"};
    private final static String CODE_QUALITY_CMD_RE = "^\\toncever run codequality*";
    private final static String ONCEOVER_DETECT = "spec/onceover.yaml";
    private final static String MAKEFILE_LABLE_RE = "^\\w+:\\s?";

    public Task(@ComponentImport final ProcessService processService,
                @ComponentImport final TestCollationService testCollationService)
    {
        this.processService = processService;
        this.testCollationService = testCollationService;
    }

    private List<String> onceoverCommand(TaskContext taskContext)
    {
        ConfigurationMap config = taskContext.getConfigurationMap();

        File makefile = new File(taskContext.getWorkingDirectory(), MAKEFILE);

        String[] command;
        if (config.get(Configurator.RUNTYPE).equals(Configurator.RUNTYPE_AUTO))
        {
            // see if we have `Makefile`
            command = (makefile.isFile()) ?
                    AUTO_RUN_ONCEOVER : MANUAL_RUN_ONCEOVER;
        }
        else
        {
            command = MANUAL_RUN_ONCEOVER;
        }

        return Arrays.asList(command);
    }

    private boolean codeQualityAlreadyInMakefile(TaskContext taskContext) {
        BuildLogger buildLogger = taskContext.getBuildLogger();
        File makefile = new File(taskContext.getWorkingDirectory(), MAKEFILE);

        boolean found = false;
        boolean insideDefaultTask = false;

        if (makefile.isFile())
        {
            try {
                for (String line : FileUtils.readLines(makefile, "UTF8"))
                {
                    if (line.matches(MAKEFILE_LABLE_RE))
                    {
                        if (insideDefaultTask) {
                            // finish the parse if we are entering subsequent tasks
                            // we only deal with the default (first) one
                            break;
                        }
                        else
                        {
                            insideDefaultTask = true;
                        }
                    }
                    if (line.matches(CODE_QUALITY_CMD_RE))
                    {
                        found = true;
                    }
                }
            }
            catch (IOException e)
            {
                buildLogger.addErrorLogEntry(String.format("Unable to read %s", MAKEFILE),e);
            }
        }
        return found;
    }

    private boolean isOnceoverConfigured(TaskContext taskContext)
    {
        File onceoverDetect = new File(taskContext.getWorkingDirectory(), ONCEOVER_DETECT);
        return onceoverDetect.isFile();
    }

    private TaskResult runCommand(TaskResultBuilder taskResultBuilder, TaskContext taskContext, List<String> prefix, List<String> command) {

        List<String> finalCommand = new ArrayList<>();
        if (prefix != null)
        {
            finalCommand.addAll(prefix);
        }
        finalCommand.addAll(command);

        return runCommand(taskResultBuilder, taskContext, finalCommand);
    }

    private TaskResult runCommand(TaskResultBuilder taskResultBuilder, TaskContext taskContext, List<String> command) {


        taskContext.getBuildLogger().addBuildLogEntry("Running command: " + String.join(" ", command));
        ExternalProcess process = processService.createExternalProcess(
                taskContext,
                new ExternalProcessBuilder()
                        .command(command)
                        .workingDirectory(taskContext.getWorkingDirectory())
        );

        process.execute();

        return taskResultBuilder.checkReturnCode(process, 0).build();
    }

    @Nonnull
    @Override
    public TaskResult execute(@Nonnull TaskContext taskContext) throws TaskException {

        BuildLogger buildLogger = taskContext.getBuildLogger();
        ConfigurationMap config = taskContext.getConfigurationMap();

        TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(taskContext);
        TaskResult taskResult =  taskResultBuilder.success().build();
        ExternalProcessLogInterceptor externalProcessLogInterceptor = new ExternalProcessLogInterceptor();

        // licence check
        String licenseStatus = taskContext
                .getRuntimeTaskContext()
                .get(EntitlementCheck.LICENSE_MESSAGE_KEY);

        if (! (EntitlementCheck.LICENSE_MESSAGE.get(licenseStatus) == null)) {
            buildLogger.addErrorLogEntry(
                    "*** " +
                    EntitlementCheck.LICENSE_MESSAGE.get(licenseStatus) +
                    " ***"
            );
        }

        // windows check
        if (SystemUtils.IS_OS_WINDOWS){
            buildLogger.addErrorLogEntry(
                    "**************************************" +
                      "* Windows is not supported           *" +
                      "* Task will continue but likely fail *" +
                      "**************************************"
            );
        }

        if (isOnceoverConfigured(taskContext))
        {

            // ruby check (to caputure output to logs)
            taskResult = runCommand(
                    taskResultBuilder,
                    taskContext,
                    Arrays.asList(RUBY_VERSION)
            );

            List<String> commandPrefix;
            if (taskResult.getTaskState() == TaskState.SUCCESS &&
                    Boolean.valueOf(config.get(Configurator.USE_BUNDLER)))
            {
                runCommand(
                        taskResultBuilder,
                        taskContext,
                        Arrays.asList(BUNDLER_VERSION)
                );

                // bundle install
                taskResult = runCommand(
                        taskResultBuilder,
                        taskContext,
                        Arrays.asList(BUNDLE_INSTALL)
                );
                commandPrefix = Arrays.asList(BUNDLE_EXEC);
            }
            else
            {
                commandPrefix = null;
            }

            if (taskResult.getTaskState() == TaskState.SUCCESS) {
                // system ready

                // there is no way to capture output directly from process without introspection
                // so we have to capture its logs instead and then stop capturing after completion
                buildLogger.getInterceptorStack().add(externalProcessLogInterceptor);
                Date onceoverStarted = new Date();

                // code quality
                if (Boolean.valueOf(config.get(Configurator.RUN_CODE_QUALITY))) {
                    if (!codeQualityAlreadyInMakefile(taskContext)) {
                        taskResult = runCommand(
                                taskResultBuilder,
                                taskContext,
                                commandPrefix,
                                Arrays.asList(MANUAL_RUN_CODE_QUALITY)
                        );
                    }
                }

                if (taskResult.getTaskState() == TaskState.SUCCESS) {
                    // main onceover
                    taskResult = runCommand(
                            taskResultBuilder,
                            taskContext,
                            commandPrefix,
                            onceoverCommand(taskContext)
                    );
                } else {
                    buildLogger.addErrorLogEntry("running codequailty failed");
                }

                // onceover + code quailty finished - turn off output processing
                buildLogger.getInterceptorStack().remove(externalProcessLogInterceptor);

                // now we feed the captured output to our test collator
                OnceoverReportProvider onceoverReportProvider = new OnceoverReportProvider(
                        externalProcessLogInterceptor.getOutput(),
                        onceoverStarted
                );
                testCollationService.collateTestResults(
                        taskContext,
                        onceoverReportProvider
                );
            }
            else
            {
                buildLogger.addErrorLogEntry("running bundle install failed");
            }
        }
        else
        {
            buildLogger.addErrorLogEntry("Repository is not configured for onceover");
            taskResult = taskResultBuilder.failedWithError().build();
        }

        return taskResult;
    }
}
