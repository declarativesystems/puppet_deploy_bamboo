package com.declarativesystems.bamboo.puppetdeploy.onceover;

import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.build.test.TestCollectionResultBuilder;
import com.atlassian.bamboo.build.test.TestReportProvider;
import com.atlassian.bamboo.results.tests.TestResults;
import com.atlassian.bamboo.resultsummary.tests.TestCaseResultErrorImpl;
import com.atlassian.bamboo.resultsummary.tests.TestState;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.*;

/**
 * https://developer.atlassian.com/server/bamboo/test-collection-and-reporting/
 */
public class OnceoverReportProvider implements TestReportProvider {
    private TestCollectionResultBuilder builder = new TestCollectionResultBuilder();
    private Collection<TestResults> successfulTestResults = Lists.newArrayList();
    private Collection<TestResults> failingTestResults = Lists.newArrayList();

    private List<String> programOutput;
    private long testDuration;

    private TestResults parsedTest;
    private StringBuilder testOutput;

    private int section = POS_START;

    private static final String TEST_FAILED     = "F";
    private static final String CODE_QUALITY_OK = "OK";
    private static final int POS_START          = 0;
    private static final int POS_CODE_QUALITY   = 1;
    private static final int POS_R10K           = 2;
    private static final int POS_SUMMARY        = 3;
    private static final int POS_ERRORS         = 4;
    private static final int POS_END            = 5;

    private static final String START_CODE_QUALITY = "Running Code Quality tests";
    private static final String START_R10K = "Using Puppetfile";
    private static final String START_SUMMARY = "rspec --pattern";
    private static final String CODE_QUALITY_TEST = "Code Quality: ";



    /**
     * Lines containing test results start with ANSI bright white terminal
     * escape code
     */
    private static final String START_TEST_NAME = "\u001B[1m";
    private static final String END_TEST_NAME="\u001B[0m";
    private static final String START_TEST_REASON = "    ";

    private static final String CODE_QUALITY_LEADER = "...";
    private static final String START_CODE_QUALITY_TEST = "Checking ";
    private static final String END_CODE_QUALITY_TEST = CODE_QUALITY_LEADER;
    private static final String END_CODE_QUALITY = "Code Quality tests";


    /**
     *
     * @param programOutput Intercepted program output, each member is one line
     * @param testStartTime Time we started running onceover
     */
    public OnceoverReportProvider(List<String> programOutput, Date testStartTime)
    {
        testDuration = new Date().getTime() - testStartTime.getTime();
        this.programOutput = programOutput;
        process();
    }

    private String stripConsoleEscapes(String rawLine)
    {
        return rawLine.replaceAll("\u001B\\[\\d\\d?\\w", "");
    }

    private String stripLogInfo(String rawLine)
    {
        // Onceover output looks like this:
        // [32mINFO[0m	 -> Checking Gen...
        // Assuming we already removed the console control characters we
        // Just want everything to the right of `->` minus one leading space
        String[] split = rawLine.split("->",2);
        return (split.length == 2) ? split[1].substring(1) : rawLine;
    }

    private void addParsedTest()
    {
        if (parsedTest != null)
        {
            // at this point the parse is complete so load it into the test results
            // we use the example from https://bitbucket.org/atlassian/bamboo-tap-plugin
            // to figure this out.
            // For test results, the fields className and methodName MUST be set even
            // though they dont make much sense ;-) to us. The GUI uses the method name
            // to render a link. Whatever text is loaded into as an error is displayed in
            // a `pre` but using mulitiple `addError()` calls doesn't do anything for
            // formatting except break it so we just format our own big string
            if (parsedTest.getState() == TestState.SUCCESS)
            {
                successfulTestResults.add(parsedTest);
            }
            else
            {
                parsedTest.addError(
                        new TestCaseResultErrorImpl(
                                testOutput.toString()
                        )
                );
                failingTestResults.add(parsedTest);
            }
        }
    }

    private void processCodeQuality(String rawLine)
    {
        String line = stripLogInfo(stripConsoleEscapes(rawLine));

        if (line.startsWith(START_CODE_QUALITY_TEST))
        {
            addParsedTest();

            // started a new test
            String testName = line
                    .replace(START_CODE_QUALITY_TEST,"")
                    .replace(CODE_QUALITY_LEADER, "");

            testOutput = new StringBuilder();
            parsedTest = new TestResults(
                    CODE_QUALITY_TEST + testName,
                    CODE_QUALITY_TEST + testName,
                    testDuration
            );
        }
        else if (line.startsWith(END_CODE_QUALITY_TEST))
        {
            // test result/end of test
            String result = line.replaceAll(CODE_QUALITY_LEADER, "");
            TestState testState = result.equals(CODE_QUALITY_OK) ?
                    TestState.SUCCESS : TestState.FAILED;
            parsedTest.setState(testState);
        }
        else
        {
            // captured error from program
            testOutput.append(line.trim()).append("\n");
        }
    }

    private void processR10k(String rawLine)
    {
        String line = stripConsoleEscapes(rawLine);
        if (parsedTest == null)
        {
            // start of output
            testOutput = new StringBuilder();
            parsedTest = new TestResults(
                    "R10K",
                    "R10K",
                    testDuration
            );
            parsedTest.setState(TestState.SUCCESS);
        }
        else
        {
            testOutput.append(line).append("\n");

            // mark R10K as failed
            if (line.startsWith("ERROR"))
            {
                parsedTest.setState(TestState.FAILED);
            }
        }
    }

    private void processSummary(String line)
    {
        if (line.startsWith(START_TEST_NAME))
        {
            // (probably) found a test result - parse the line
            String testClass = line.substring(
                    START_TEST_NAME.length(),
                    line.indexOf(END_TEST_NAME)
            );

            String rawResults = line.substring(
                    line.indexOf(END_TEST_NAME) + END_TEST_NAME.length() - 1,
                    line.length() -1
            );

            // testResults contains a bunch of terminal control characters to set
            // red/green text - we can throw all this away and just leave the P/F
            // characters. We need to preserve order since this will be useful
            // when results are fully tabularized with the testcase name
            String results = rawResults.replaceAll("[^PF]", "");

            // If there no failures then the test passed. If there are failures, then
            // we need to handle these separately as the detail section at the end of
            // output contains the failure reasons
            if (! results.contains(TEST_FAILED))
            {
                TestResults testResults = new TestResults(
                        testClass,
                        testClass,
                        testDuration);

                testResults.setState(TestState.SUCCESS);
                successfulTestResults.add(testResults);
            }
        }
    }

    private void processErrorDetail(String rawLine)
    {
        // we process line-at-a-time but errors span several lines:
        //[1mprofile::linux::at[0m: [31mfailed[0m
        //  errors:
        //    [31mEvaluation Error: Cannot reassign variable '$oh'[0m
        //      file: [1msite/profile/manifests/linux/at.pp[0m
        //      line: [1m14[0m
        //      column: [1m9[0m
        //      factsets: [1mAmazon-2018.03_agent_NonProduction, Amazon-2018.03_agent_Production[0m
        //
        // since there is consistent spacing we can use this to figure out where we
        // are in the current error
        String line = stripConsoleEscapes(rawLine);
        if (! line.startsWith(" "))
        {
            addParsedTest();
            String testClass = line.split(" ")[0];
            parsedTest = new TestResults(
                    testClass,
                    testClass,
                    testDuration
            );
            parsedTest.setState(TestState.FAILED);
            testOutput = new StringBuilder();
        }
        // only grab the 'right' indent of text
        else if (line.startsWith(START_TEST_REASON))
        {
            testOutput.append(line.trim()).append("\n");
        }
    }

    private void finishParseSection()
    {
        switch (section) {
            case POS_CODE_QUALITY:
            case POS_R10K:
            case POS_ERRORS:
                addParsedTest();
                break;
            case POS_START:
            case POS_SUMMARY:
            case POS_END:
                // no action needed
                break;
            default:
                throw new RuntimeException(
                        "requested finishParseSection() for unknown section: " + section
                );
        }
        parsedTest = null;
        testOutput = null;
        section++;
    }

    /**
     * Extract the test results from the captured output. Results look like this
     * profile::linux::at:                 P P P F
     * profile::linux::cron:               P P P P
     *
     * End of test results is denoted by 2 consecutive blank lines and there are
     * a ton of console control characters
     */
    private void process()
    {
        for (int i = 0 ; i < programOutput.size() ; i++)
        {
            String line = programOutput.get(i);

            if (section == POS_START && line.contains(START_CODE_QUALITY))
            {
                // enter code quality tests
                finishParseSection();
            }
            else if (section == POS_CODE_QUALITY && line.contains(END_CODE_QUALITY))
            {
                // finish code quality tests
                finishParseSection();
            }
            else if (section == POS_START && line.contains(START_R10K))
            {
                // start of R10K (jump over code quality)
                finishParseSection();
                finishParseSection();
            }
            else if (section == POS_CODE_QUALITY && line.contains(START_R10K))
            {
                // start of R10K
                finishParseSection();
            }
            else if (section == POS_R10K && line.contains(START_SUMMARY))
            {
                // start of rspec summary
                finishParseSection();
            }
            // 2x blank lines - detailed test results
            else if (section == POS_SUMMARY && i>0 && StringUtils.isBlank(line) && StringUtils.isBlank(programOutput.get(i-1)))
            {
                finishParseSection();
            }
            // 1x blank line after parsing errors is the end of data
            else if (section == POS_ERRORS && StringUtils.isBlank(line))
            {
                finishParseSection();
                break;
            }
            else {
                switch (section) {
                    case POS_CODE_QUALITY:
                        processCodeQuality(line);
                        break;
                    case POS_R10K:
                        processR10k(line);
                        break;
                    case POS_SUMMARY:
                        processSummary(line);
                        break;
                    case POS_ERRORS:
                        processErrorDetail(line);
                        break;
                    case POS_START:
                        // inside output but haven't entered a section yet
                        // (comment or other program output)
                        break;
                    default:
                        throw new RuntimeException(
                                "Error parsing onceover results, unknown section: " + section
                        );
                }
            }
        }
        // always finish what we were doing...
        finishParseSection();
    }

    @NotNull
    @Override
    public TestCollectionResult getTestCollectionResult()
    {
        return builder
                .addSuccessfulTestResults(successfulTestResults)
                .addFailedTestResults(failingTestResults)
                .build();
    }
}
