package com.declarativesystems.bamboo.puppetdeploy.onceover;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.TaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.declarativesystems.atlassian.EntitlementCheck;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

public class Configurator implements TaskConfigurator {
    private EntitlementCheck entitlementCheck;

    public static final String RUNTYPE_AUTO         = "auto";
    public static final String RUNTYPE_MANUAL       = "manual";
    public static final String RUNTYPES             = "runTypes";
    public static final String RUNTYPE              = "runType";
    public static final String USE_BUNDLER          = "useBundler";
    public static final String RUN_CODE_QUALITY     = "runCodeQuality";

    @Inject
    public Configurator(EntitlementCheck entitlementCheck) {
        this.entitlementCheck = entitlementCheck;
    }

    // On the off chance this ever needs to i18n'ed
    // https://developer.atlassian.com/server/framework/atlassian-sdk/internationalising-your-plugin/
    private static final Map<String,String> runTypes = new HashMap<String,String>() {{
        put(RUNTYPE_AUTO, "Autodetect");
        put(RUNTYPE_MANUAL, "onceover run spec");
    }};

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> map, @NotNull TaskDefinition taskDefinition) {
        map.put(USE_BUNDLER, taskDefinition.getConfiguration().get(USE_BUNDLER));
        map.put(RUN_CODE_QUALITY, taskDefinition.getConfiguration().get(RUN_CODE_QUALITY));
        map.put(RUNTYPES, runTypes);
        map.put(RUNTYPE, taskDefinition.getConfiguration().get(RUNTYPE));

        map.put(EntitlementCheck.LICENSE_MESSAGE_KEY,
                EntitlementCheck.LICENSE_MESSAGE.get(
                        entitlementCheck.getLicenseMessage()
                )
        );

    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> map) {
        map.put(RUNTYPES, runTypes);
        map.put(USE_BUNDLER, "true");
        map.put(RUN_CODE_QUALITY, "true");

        // select the automatic by default
        map.put(RUNTYPE, RUNTYPE_AUTO);

        map.put(EntitlementCheck.LICENSE_MESSAGE_KEY,
                EntitlementCheck.LICENSE_MESSAGE.get(
                        entitlementCheck.getLicenseMessage()
                )
        );

    }

    @Override
    public void validate(@NotNull ActionParametersMap actionParametersMap, @NotNull ErrorCollection errorCollection) {

    }

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap actionParametersMap, @Nullable TaskDefinition taskDefinition) {
        Map<String, String> config = new HashMap<>();
        config.put(RUNTYPE, actionParametersMap.getString(RUNTYPE));
        config.put(USE_BUNDLER, actionParametersMap.getString(USE_BUNDLER));
        config.put(RUN_CODE_QUALITY, actionParametersMap.getString(RUN_CODE_QUALITY));
        return config;
    }
}
