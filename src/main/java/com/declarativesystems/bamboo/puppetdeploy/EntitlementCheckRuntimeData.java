package com.declarativesystems.bamboo.puppetdeploy;

import com.atlassian.bamboo.task.RuntimeTaskDataProvider;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.runtime.RuntimeTaskDefinition;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.plugin.spring.scanner.annotation.component.BambooComponent;
import com.declarativesystems.atlassian.EntitlementCheck;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@BambooComponent
public class EntitlementCheckRuntimeData implements RuntimeTaskDataProvider {


    private EntitlementCheck entitlementCheck;

    @Inject
    public EntitlementCheckRuntimeData(EntitlementCheck entitlementCheck)
    {
        this.entitlementCheck = entitlementCheck;
    }

    @NotNull
    @Override
    public Map<String, String> populateRuntimeTaskData(@NotNull TaskDefinition taskDefinition, @NotNull CommonContext commonContext) {
        Map<String, String> data = new HashMap<>();
        data.put(
                EntitlementCheck.LICENSE_MESSAGE_KEY,
                entitlementCheck.getLicenseMessage()
        );

        return data;
    }

    @Override
    public void processRuntimeTaskData(@NotNull TaskDefinition taskDefinition, @NotNull CommonContext commonContext) {

    }

    @Override
    public void processRuntimeTaskData(@NotNull RuntimeTaskDefinition runtimeTaskDefinition, @NotNull CommonContext commonContext) {

    }
}
