package com.declarativesystems.bamboo.puppetdeploy;

import com.atlassian.bamboo.build.LogEntry;
import com.atlassian.bamboo.build.logger.LogInterceptor;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class ExternalProcessLogInterceptor implements LogInterceptor {
    private List<String> output = new ArrayList<>();

    @Override
    public void intercept(@NotNull LogEntry logEntry)
    {
        output.add(logEntry.getUnstyledLog());
    }

    @Override
    public void interceptError(@NotNull LogEntry logEntry)
    {
        output.add(logEntry.getUnstyledLog());
    }

    public List<String> getOutput()
    {
        return output;
    }
}
