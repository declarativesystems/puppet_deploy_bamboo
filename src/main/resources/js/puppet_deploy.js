/*
 * Copyright 2017 Declarative Systems PTY LTD
 * Copyright 2016 Puppet Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
define('puppet_deploy/config', [
    'exports',
    'jquery'
], function (exports, $) {

    var STATUS_OK = "ok";
    var STATUS_PUPPET_ERROR = "puppet_error";
    var REPLY_FIELD = "reply";
    var REQUEST_ID_FIELD = "requestId";
    var STATUS_FIELD = "status";
    var OUTPUT_FIELD = "htmlTableRows";

    // detect if we are running in bitbucket or bamboo - bitbucket javascript
    // gives us a variable called `bitbucket`
    var SERVER_TYPE = (typeof bitbucket !== 'undefined') ? "Bitbucket" : "Bamboo";
    var buttonId = '#deployNow';
    var RESULT_TABLE_SOURCE = '#puppetResultTableSource';

    var STATUS_MESSAGE = {
        "request_error": "There was a problem with the data you supplied",
        "internal_error": SERVER_TYPE + " encountered an error"
    };
    STATUS_MESSAGE[STATUS_OK] = "Puppet Enterprise deployment OK";
    STATUS_MESSAGE[STATUS_PUPPET_ERROR] = "Puppet Enterprise reported an error";


    // resolve the environment from the UI - to deploy all environments
    // pass empty string/null otherwise make sure the user set something
    function resolveEnvironment() {
        var selectedEnvironment = AJS.$('#selectedEnvironment').val().split(/\//).pop();;
        var deployEnvironment = AJS.$("input[name='deployEnvironment']:checked").val();
        var environment;

        if (deployEnvironment == "all") {
            environment = null;
        } else if (deployEnvironment == "specific" && selectedEnvironment.match(/\w+/)) {
            environment = selectedEnvironment;
        } else {
            environment = false;
        }

        return environment;
    }


    function getResultTableHtml(tableRows) {
        var htmltxt;
        if (tableRows) {
            var r = AJS.$(RESULT_TABLE_SOURCE);
            var resultTable = r.clone(true);
            resultTable.find('tbody').append(tableRows);

            htmltxt = resultTable.html();
        } else {
            htmltxt = "";
        }
        return htmltxt;
    }

    function getJsonHtml(tableRows, jsonReply) {
        //var jsonReply = JSON.parse(jsonReplyRaw);
        //jsonPretty = JSON.stringify(jsonReply, null, 2);

        var preClass = tableRows ? "puppetJsonReply" : "puppetReply";

        return '<pre class="' + preClass + '" >' + jsonReply + '</pre>';
    }

    /**
     * Compare the supplied target semver version (eg 6.0.2) to version reported
     * by AJS.version
     * @param target
     * @return
     *  -1 `AJS.version` < `query`
     *  +1 `AJS.version` >= `query`
     */
    function puppetdeploy_compareAuiVersion(query) {
        var comparison;
        var querySemver = query.split(".");
        var auiSemver = AJS.version.split(".");

        var major = 0;
        var minor = 1;
        var patch = 2;

        var lt = -1;
        var gte = 1;

        if (auiSemver[major] > querySemver[major]) {
            comparison = gte;
        } else if (auiSemver[major] === querySemver[major]) {
            if (auiSemver[minor] > querySemver[minor]) {
                comparison = gte;
            } else if (auiSemver[minor] === querySemver[minor]) {
                if (auiSemver[patch] > querySemver[patch]) {
                    comparison = gte;
                } else if (auiSemver[patch] == querySemver[patch]) {
                    comparison = gte;
                } else {
                    comparison = lt;
                }
            } else {
                comparison = lt;
            }
        } else {
            comparison = lt;
        }
        return comparison;
    }

    function disableDeployButton(button) {
        // in-button spinner needs AUI >= 7.9.9 - users with an older bitbucket
        // just get a disabled button while the query runs
        if (puppetdeploy_compareAuiVersion("7.9.9") > 0) {
            button.busy();
        }

        AJS.$(button).prop("disabled", true);
    }


    function enableDeployButton(button) {
        if (puppetdeploy_compareAuiVersion("7.9.9") > 0) {
            button.idle();
        }
        AJS.$(button).prop("disabled", false);
    }


    // export the onReady function that the javascript in the soy
    // template will call to bootstrap everything
    exports.onReady = function () {
        // wire the deploy now button + ajax
        AJS.$(buttonId).click(function () {
            var that = this;
            disableDeployButton(that);

            var environment = resolveEnvironment();
            if (environment === false) {
                AJS.messages.error('#puppetDeployMessageBar', {
                    title: "Must specify environment",
                    body: "You must specify an environment to deploy if not deploying all environments"
                });
                enableDeployButton(that);
            } else {
                var data = {};
                data['puppetMasterFqdn'] = AJS.$('#puppetMasterFqdn').val();
                data['requestId'] = Math.random().toString();
                data['token'] = AJS.$('#token').val();
                data['caCert'] = encodeURIComponent(AJS.$('#caCert').val());
                data['environment'] = environment;

                var jqxhr = AJS.$.post(AJS.contextPath() + '/plugins/servlet/puppet_deploy/configurePlugin', JSON.stringify(data), "json")
                    .done(function (resp) {
                        var title;
                        var message;


                        if (resp[STATUS_FIELD] === STATUS_OK || resp[STATUS_FIELD] === STATUS_PUPPET_ERROR) {
                            // we got some kind of response back from puppet

                            // something = window.open("data:text/json," + encodeURIComponent(jsonReply),
                            //     "_blank");
                            // something.focus();



                            if (jqxhr.requestId !== resp[REQUEST_ID_FIELD]) {
                                // nonce doesn't match
                                title = "Reply from " + SERVER_TYPE + " doesn't match our requestId";
                                message = "<i>Reply doesn't match this request, check for unwanted caching of " + SERVER_TYPE + " server</i><br />";
                            } else {
                                title = STATUS_MESSAGE[resp[STATUS_FIELD]];
                                message = getResultTableHtml(resp[OUTPUT_FIELD]) + getJsonHtml(resp[OUTPUT_FIELD], resp[REPLY_FIELD]);
                            }
                        } else {
                            // comms error
                            title = STATUS_MESSAGE[resp[STATUS_FIELD]];
                            message = '<pre class="puppetReply">' + AJS.escapeHtml(resp["reply"]) + '</pre>'
                        }


                        if (resp[STATUS_FIELD] === STATUS_OK) {
                            AJS.messages.success('#puppetDeployMessageBar', {
                                title: title,
                                body: message
                            });
                        } else {
                            AJS.messages.error('#puppetDeployMessageBar', {
                                title: title,
                                body: message //+ detail + debug
                            });
                        }

                        // after printing the table add the atlassian lozenge classes...
                        AJS.$('.puppetOk').addClass('aui-lozenge').addClass('aui-lozenge-success');

                        // error is deprecated but removed doesn't work yet
                        AJS.$('.puppetError').addClass('aui-lozenge').addClass('aui-lozenge-removed').addClass('aui-lozenge-error');
                    })
                    .fail(function (jqXHR, textStatus) {
                        var message;
                        var title;
                        if (textStatus === "parsererror" && jqXHR.status === 200) {
                            title = "There was a problem with the reply from " + SERVER_TYPE + " Server";
                            message = "Invalid JSON data received (parsererror) <br />";
                        } else {
                            title = "There was a problem reaching " + SERVER_TYPE + " Server";

                            // eg "Requested - GET /foo/bar/baz
                            message = "Request - " + this.type + ":  " + this.url + "<br />";

                            // eg "error 404"
                            message += "Response - " + textStatus + " " + jqXHR.status + "<br />";
                        }
                        if (jqXHR.responseText) {
                            message += 'Raw message from ' + SERVER_TYPE + ':  <br /><pre class="puppetReply">' + AJS.escapeHtml(jqXHR.responseText) + '</pre>';
                        } else {
                            message += "<i>Empty response received</i>";
                        }

                        AJS.messages.error('#puppetDeployMessageBar', {
                            title: title,
                            body: message
                        });
                    })
                    .always(function () {
                        enableDeployButton(that);
                    });

                // store the request ID this worker was supposed to be servicing so we can check we received correct reply
                jqxhr.requestId = data['requestId'];
            }
        });

        // Unprotect the token field (stop browser inserting wrong values)
        setTimeout(function()
        {
            AJS.$('#token').attr('readonly', null);
            AJS.$('#token').attr('disabled', null);
        }, 1000);

    };
});