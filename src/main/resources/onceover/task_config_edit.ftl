[#if .data_model["com.declarativesystems.bamboo.puppetdeploy.LICENSE_STATUS"]??]
    <span class="aui-lozenge aui-lozenge-error">
        ${.data_model["com.declarativesystems.bamboo.puppetdeploy.LICENSE_STATUS"]}
    </span>
[/#if]

<h1>Onceover</h1>
[@ww.checkbox name="useBundler" descriptionKey="puppet_deploy.onceover.useBundler"/]
[@ww.checkbox name="runCodeQuality" descriptionKey="puppet_deploy.onceover.runCodeQuality"/]

<br />
<lable>How to run onceover</lable>
[@ww.radio name="runType"
    list=runTypes
    listKey='key'
    listValue='value'
    cssClass="radio-group" /]


