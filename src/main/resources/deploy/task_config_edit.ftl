${webResourceManager.getResourceTags("com.declarativesystems.bamboo.puppetdeploy:puppet_deploy-resources")}

[#if .data_model["com.declarativesystems.bamboo.puppetdeploy.LICENSE_STATUS"]??]
    <span class="aui-lozenge aui-lozenge-error">
        ${.data_model["com.declarativesystems.bamboo.puppetdeploy.LICENSE_STATUS"]}
    </span>
[/#if]

<h1>Puppet Deploy</h1>
<h2>Settings</h2>

[@ww.textfield labelKey="puppet_deploy.deploy.puppetMasterFqdn" name="puppetMasterFqdn" required='true'/]
[@ww.label labelKey='puppet_deploy.deploy.hint.puppetMasterFqdn'/]
[@ww.password showPassword="true" labelKey="puppet_deploy.deploy.token" name="token" required='true' readonly="true"/]
[@ww.label labelKey='puppet_deploy.deploy.hint.token'/]
[@ww.textarea labelKey='puppet_deploy.deploy.caCert' name='caCert' rows='10' cols='200'/]
[@ww.label labelKey='puppet_deploy.deploy.hint.caCert'/]
[@ww.textfield labelKey="puppet_deploy.deploy.skipEnvironments" name="skipEnvironments"/]
[@ww.label labelKey='puppet_deploy.deploy.hint.skipEnvironments'/]

<h2>Actions</h2>
[@ww.label labelKey="puppet_deploy.deploy.actions" /]

<div class="radio">
    <input class="radio" type="radio"
           name="deployEnvironment" id="deployEnvironmentAll"
           value="all" checked="checked"
           onchange="AJS.$('#selectBranchDiv').hide()"
    />
    <label for="deployEnvironmentAll">All environments</label>
    <br/>
    <input class="radio" type="radio"
           name="deployEnvironment" id="deployEnvironmentSpecific"
           value="specific"
           onchange="AJS.$('#selectBranchDiv').show()"
    />
    <label for="deployEnvironmentSpecific">Specific environment:</label>
    <div id="selectBranchDiv" class="puppetBranchDiv">
        [@ww.textfield name="selectedEnvironment" value="" /]<br/>
    </div>
</div>

<br />
<button type="button" class="aui-button" id="deployNow">Deploy</button>

<div id="puppetDeployMessageBar">&nbsp;</div>
<div id="puppetResultTableSource" style="display:none;">
    <table class="aui">
        <thead>
        <tr>
            <th>Environment</th>
            <th>Status</th>
            <th>Deploy signature</th>
        </tr>
        </thead>
        <tbody id="puppetDeployRows"/>
    </table>
    <button id="json" type="button" class="aui-button" onclick="AJS.$(this).siblings('.puppetJsonReply').toggle() ">Toggle Code Manager JSON</button>

</div>


<script type="text/javascript">
    require('puppet_deploy/config').onReady();
</script>