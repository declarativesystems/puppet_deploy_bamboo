package test.com.declarativesystems;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.upm.api.license.PluginLicenseManager;
import com.declarativesystems.atlassian.EntitlementCheck;
import com.declarativesystems.atlassian.EntitlementCheckImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import test.com.declarativesystems.mock.MockPluginLicenseManager;
import test.com.declarativesystems.mock.MockPluginLicenseManagerImpl;

import static org.junit.Assert.*;

import javax.inject.Inject;


public class EntitlementCheckTest {

    private EntitlementCheck entitlementCheck;
    private PluginLicenseManager mockPluginLicenseManager;

    public EntitlementCheckTest() {
        this.mockPluginLicenseManager = new MockPluginLicenseManagerImpl();
        this.entitlementCheck = new EntitlementCheckImpl(
                mockPluginLicenseManager
        );
    }


    // @Test
    public void testLicensed() throws Exception {
        ((MockPluginLicenseManager) mockPluginLicenseManager).setLicensed(true);

        assertNull(
                "No license message when licensed",
                entitlementCheck.getLicenseMessage()
        );
    }

    // @Test
    public void testUnlicensed() throws Exception {
        ((MockPluginLicenseManager) mockPluginLicenseManager).setLicensed(false);

        assertNotNull(
                "Message when unlicensed",
                entitlementCheck.getLicenseMessage()
        );
    }
}
