package test.com.declarativesystems.deploy;

import com.declarativesystems.bamboo.puppetdeploy.deploy.ConfigServlet;
import com.declarativesystems.pejava.codemanager.Deploy;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import test.com.declarativesystems.mock.MockDeploy;
import test.com.declarativesystems.mock.UserManager;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.util.Map;
import java.util.Vector;

import static org.junit.Assert.assertEquals;

public class ConfigServletTest extends Mockito {
    private Deploy mockDeploy = new MockDeploy();

    @Test
    public void testLoggedOutNoAccess() throws Exception {
        // mocks for servlet request/response so that we can test interaction with
        // them vs setting up a whole servlet container to do this
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        // test.com.declarativesystems.mock the URL we accessed from
        when(request.getRequestURL()).thenReturn(new StringBuffer("http://fake/bamboo"));

        // test.com.declarativesystems.mock/capture the servlet output stream
        StringWriter servletBuffer = new StringWriter();
        PrintWriter writer = new PrintWriter(servletBuffer);
        when(response.getWriter()).thenReturn(writer);

        // a real servlet with mocks for the parts that normally come from bamboo
        new ConfigServlet(
                new UserManager(UserManager.LOGGED_OUT),
                mockDeploy
        ).doPost(request, response);

        // check no output in client stream
        writer.flush();

        // parse JSON received from sevlet and check we get a permission error
        Map<String, String> data = new Gson().fromJson(servletBuffer.toString(),
                new TypeToken<Map<String, String>>() {
                }.getType());
        assertEquals(ConfigServlet.STATUS_PERMISSION_ERROR, data.get(ConfigServlet.STATUS_FIELD));
    }

    @Test
    public void testNonAdminNoAccess() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        when(request.getRequestURL()).thenReturn(new StringBuffer("http://fake/bitbucket"));

        StringWriter servletBuffer = new StringWriter();
        PrintWriter writer = new PrintWriter(servletBuffer);
        when(response.getWriter()).thenReturn(writer);

        new ConfigServlet(
                new UserManager(UserManager.USER),
                mockDeploy
        ).doPost(request, response);

        // parse JSON received from sevlet and check we get a permission error
        Map<String, String> data = new Gson().fromJson(servletBuffer.toString(),
                new TypeToken<Map<String, String>>() {
                }.getType());
        assertEquals(ConfigServlet.STATUS_PERMISSION_ERROR, data.get(ConfigServlet.STATUS_FIELD));
    }

    @Test
    public void testBadRequest() throws Exception {

        URL url = this.getClass().getResource("/mock_request_bad.json");

        String mockJson = FileUtils.readFileToString(new File(url.getFile()), "UTF-8");
        Vector<String> mockResponse = new Vector<>();
        mockResponse.add(mockJson);
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        when(request.getRequestURL()).thenReturn(new StringBuffer("http://fake/bitbucket"));

        when(request.getParameterNames()).thenReturn(mockResponse.elements());

        // test.com.declarativesystems.mock/capture the servlet output stream
        StringWriter servletBuffer = new StringWriter();
        PrintWriter writer = new PrintWriter(servletBuffer);
        when(response.getWriter()).thenReturn(writer);

        // a real servlet with mocks for the parts that normally come from bitbucket
        new ConfigServlet(
                new UserManager(UserManager.ADMIN),
                mockDeploy
        ).doPost(request, response);

        // we are logged in so dont redirect
        ArgumentCaptor<String> sendRedirectCaptor = ArgumentCaptor.forClass(String.class);
        verify(response, times(0)).sendRedirect(sendRedirectCaptor.capture());

        // check valid json sent to client
        writer.flush();
        Map<String, String> data = new Gson().fromJson(
                servletBuffer.toString(),
                new TypeToken<Map<String, String>>() {
                }.getType()
        );

        assert(data.containsKey(ConfigServlet.REPLY_FIELD));
        assert(data.containsKey(ConfigServlet.STATUS_FIELD));
        assertEquals(ConfigServlet.STATUS_REQUEST_ERROR, data.get(ConfigServlet.STATUS_FIELD));

    }

    @Test
    public void testEmptyRequest() throws Exception {

        URL url = this.getClass().getResource("/mock_request_empty.json");

        String mockJson = FileUtils.readFileToString(new File(url.getFile()), "UTF-8");
        Vector<String> mockResponse = new Vector<>();
        mockResponse.add(mockJson);
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        when(request.getRequestURL()).thenReturn(new StringBuffer("http://fake/bitbucket"));

        when(request.getParameterNames()).thenReturn(mockResponse.elements());

        // test.com.declarativesystems.mock/capture the servlet output stream
        StringWriter servletBuffer = new StringWriter();
        PrintWriter writer = new PrintWriter(servletBuffer);
        when(response.getWriter()).thenReturn(writer);

        // a real servlet with mocks for the parts that normally come from bitbucket
        new ConfigServlet(
                new UserManager(UserManager.ADMIN),
                mockDeploy
        ).doPost(request, response);

        // we are logged in so dont redirect
        ArgumentCaptor<String> sendRedirectCaptor = ArgumentCaptor.forClass(String.class);
        verify(response, times(0)).sendRedirect(sendRedirectCaptor.capture());

        // check valid json sent to client
        writer.flush();
        Map<String, String> data = new Gson().fromJson(
                servletBuffer.toString(),
                new TypeToken<Map<String, String>>() {
                }.getType()
        );

        assert(data.containsKey(ConfigServlet.REPLY_FIELD));
        assert(data.containsKey(ConfigServlet.STATUS_FIELD));
        assertEquals(ConfigServlet.STATUS_REQUEST_ERROR, data.get(ConfigServlet.STATUS_FIELD));

    }

    @Test
    public void testGoodRequest() throws Exception {

        URL url = this.getClass().getResource("/mock_request_good.json");

        String mockJson = FileUtils.readFileToString(new File(url.getFile()), "UTF-8");
        Vector<String> mockResponse = new Vector<>();
        mockResponse.add(mockJson);
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        when(request.getRequestURL()).thenReturn(new StringBuffer("http://fake/bitbucket"));

        when(request.getParameterNames()).thenReturn(mockResponse.elements());

        // test.com.declarativesystems.mock/capture the servlet output stream
        StringWriter servletBuffer = new StringWriter();
        PrintWriter writer = new PrintWriter(servletBuffer);
        when(response.getWriter()).thenReturn(writer);

        // a real servlet with mocks for the parts that normally come from bitbucket
        new ConfigServlet(
                new UserManager(UserManager.ADMIN),
                mockDeploy
        ).doPost(request, response);

        // we are logged in so dont redirect
        ArgumentCaptor<String> sendRedirectCaptor = ArgumentCaptor.forClass(String.class);
        verify(response, times(0)).sendRedirect(sendRedirectCaptor.capture());

        // check valid json sent to client
        writer.flush();
        Map<String, String> data = new Gson().fromJson(
                servletBuffer.toString(),
                new TypeToken<Map<String, String>>() {
                }.getType()
        );

        assert(data.containsKey(ConfigServlet.REPLY_FIELD));
        assert(data.containsKey(ConfigServlet.STATUS_FIELD));
        assertEquals(ConfigServlet.STATUS_OK, data.get(ConfigServlet.STATUS_FIELD));
    }
}