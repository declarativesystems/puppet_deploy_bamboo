package test.com.declarativesystems.onceover;

import com.atlassian.bamboo.build.test.TestCollectionResult;
import com.atlassian.bamboo.results.tests.TestResults;
import com.declarativesystems.bamboo.puppetdeploy.onceover.OnceoverReportProvider;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class OnceoverReportProviderTest {
    @Test
    public void testAllPassing() throws Exception {
        List<String> data = FileUtils.readLines(new File("src/test/resources/onceover/pass_all.txt"), "UTF-8");

        OnceoverReportProvider orp = new OnceoverReportProvider(data, new Date());
        TestCollectionResult res = orp.getTestCollectionResult();

        assertEquals(
                "no failures reported",
                0,
                res.getFailedTestResults().size()
        );
        assertEquals(
                "correct test count",
                42,
                res.getSuccessfulTestResults().size()
        );
    }

    @Test
    public void testFailures() throws Exception {
        List<String> data = FileUtils.readLines(new File("src/test/resources/onceover/failures.txt"), "UTF-8");

        OnceoverReportProvider orp = new OnceoverReportProvider(data, new Date());
        TestCollectionResult res = orp.getTestCollectionResult();

        assertEquals(
                "correct fail count",
                1,
                res.getFailedTestResults().size()
        );
        assertEquals(
                "correct test count",
                33,
                res.getSuccessfulTestResults().size()
        );
    }

    @Test
    public void testParsesNames() throws Exception {
        List<String> data = FileUtils.readLines(new File("src/test/resources/onceover/test_names.txt"), "UTF-8");

        OnceoverReportProvider orp = new OnceoverReportProvider(data, new Date());
        TestCollectionResult res = orp.getTestCollectionResult();

        // check we have 3x correct names

        TestResults testResults;

        // skip the first test which is R10K
        testResults = res.getSuccessfulTestResults().get(1);
        assertEquals(
                "test name parsed OK",
                "profile::linux::at:",
                testResults.getClassName()
        );
        assertEquals(
                "test name duped OK",
                "profile::linux::at:",
                testResults.getActualMethodName()
        );

        testResults = res.getSuccessfulTestResults().get(2);
        assertEquals(
                "test name parsed OK",
                "profile::linux::cron:",
                testResults.getClassName()
        );
        assertEquals(
                "test name duped OK",
                "profile::linux::cron:",
                testResults.getActualMethodName()
        );

        testResults = res.getSuccessfulTestResults().get(3);
        assertEquals(
                "test name parsed OK",
                "profile::linux::datadog:",
                testResults.getClassName()
        );
        assertEquals(
                "test name duped OK",
                "profile::linux::datadog:",
                testResults.getActualMethodName()
        );
    }

    @Test
    public void testFailureReason() throws Exception {
        List<String> data = FileUtils.readLines(new File("src/test/resources/onceover/failure_reason.txt"), "UTF-8");

        OnceoverReportProvider orp = new OnceoverReportProvider(data, new Date());
        TestCollectionResult res = orp.getTestCollectionResult();

        // There are 3 failures
        TestResults testResults;

        testResults = res.getFailedTestResults().get(0);
        assertEquals(
                "test name parsed from detail section OK",
                "profile::linux::at:",
                testResults.getClassName()
        );
        assertEquals(
                "parsed real error message",
                "Evaluation Error: Cannot reassign variable '$oh'\n" +
                        "file: site/profile/manifests/linux/at.pp\n" +
                        "line: 14\n" +
                        "column: 9\n" +
                        "factsets: Amazon-2018.03_agent_NonProduction, Amazon-2018.03_agent_Production\n",
                testResults.getErrors().get(0).getContent()
        );

        testResults = res.getFailedTestResults().get(1);
        assertEquals(
                "test name parsed from detail section OK",
                "role::linux::soe:",
                testResults.getClassName()
        );
        assertEquals(
                "parsed real error message",
                "Evaluation Error: Cannot reassign variable '$oh'\n" +
                        "file: site/profile/manifests/linux/at.pp\n" +
                        "line: 14\n" +
                        "column: 9\n" +
                        "factsets: Amazon-2018.03_agent_NonProduction, Amazon-2018.03_agent_Production\n",
                testResults.getErrors().get(0).getContent()
        );

        testResults = res.getFailedTestResults().get(2);
        assertEquals(
                "test name parsed from detail section OK",
                "role::soe:",
                testResults.getClassName()
        );
        assertEquals(
                "parsed real error message",
                "This 'not' expression has no effect. A value was produced and then forgotten (one or more preceding expressions may have the wrong form)\n" +
                        "file: site/role/manifests/soe.pp\n" +
                        "line: 6\n" +
                        "column: 2\n" +
                        "factsets: Amazon-2018.03_agent_NonProduction, Amazon-2018.03_agent_Production, Centos-7.0-64_agent_NonProduction, Centos-7.0-64_agent_Production, RHEL-6.7_agent_NonProduction, RHEL-6.7_agent_Production, RHEL-7.4_agent_NonProduction, RHEL-7.4_agent_Production\n",
                testResults.getErrors().get(0).getContent()
        );
    }

    @Test
    public void testFailureR10k() throws Exception {
        List<String> data = FileUtils.readLines(new File("src/test/resources/onceover/failure_r10k.txt"), "UTF-8");

        OnceoverReportProvider orp = new OnceoverReportProvider(data, new Date());
        TestCollectionResult res = orp.getTestCollectionResult();

        assertEquals(
                "correct fail count",
                1,
                res.getFailedTestResults().size()
        );
        assertEquals(
                "no tests run",
                0,
                res.getSuccessfulTestResults().size()
        );

    }

    @Test
    public void testFailureCodeQuality() throws Exception {
        List<String> data = FileUtils.readLines(new File("src/test/resources/onceover/failure_code_quality.txt"), "UTF-8");

        OnceoverReportProvider orp = new OnceoverReportProvider(data, new Date());
        TestCollectionResult res = orp.getTestCollectionResult();

        assertEquals(
                "correct fail count",
                5,
                res.getFailedTestResults().size()
        );
        assertEquals(
                "correct pass count (not 100% test failure)",
                3,
                res.getSuccessfulTestResults().size()
        );

    }
}
