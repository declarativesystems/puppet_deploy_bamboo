package test.com.declarativesystems.mock;

import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.sal.api.user.UserResolutionException;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

public class UserManager implements com.atlassian.sal.api.user.UserManager
{
    public static final int LOGGED_OUT = 0;
    public static final int USER = 1;
    public static final int ADMIN = 2;
    private int status = -1;

    public UserManager(int status) {
        this.status = status;
    }

    @Nullable
    @Override
    public String getRemoteUsername() {
        return null;
    }

    @Nullable
    @Override
    public UserProfile getRemoteUser() {
        return null;
    }

    @Nullable
    @Override
    public UserKey getRemoteUserKey() {
        UserKey userKey;
        switch (status) {
            case ADMIN: {
                userKey = new UserKey("admin");
                break;
            }
            case USER: {
                userKey = new UserKey("user");
                break;
            }
            default: {
                userKey = null;
            }
        }
        return userKey;
    }

    @Nullable
    @Override
    public String getRemoteUsername(HttpServletRequest httpServletRequest) {
        return null;
    }

    @Nullable
    @Override
    public UserProfile getRemoteUser(HttpServletRequest httpServletRequest) {
        return null;
    }

    @Nullable
    @Override
    public UserKey getRemoteUserKey(HttpServletRequest httpServletRequest) {
        return getRemoteUserKey();
    }

    @Nullable
    @Override
    public UserProfile getUserProfile(@Nullable String s) {
        return null;
    }

    @Nullable
    @Override
    public UserProfile getUserProfile(@Nullable UserKey userKey) {
        return null;
    }

    @Override
    public boolean isUserInGroup(@Nullable String s, @Nullable String s1) {
        return false;
    }

    @Override
    public boolean isUserInGroup(@Nullable UserKey userKey, @Nullable String s) {
        return false;
    }

    @Override
    public boolean isSystemAdmin(@Nullable String s) {
        return false;
    }

    @Override
    public boolean isSystemAdmin(@Nullable UserKey userKey) {
        return false;
    }

    @Override
    public boolean isAdmin(@Nullable String s) {
        return (status == ADMIN);
    }

    @Override
    public boolean isAdmin(@Nullable UserKey userKey) {
        return (status == ADMIN);
    }

    @Override
    public boolean authenticate(String s, String s1) {
        return false;
    }

    @Nullable
    @Override
    public Principal resolve(String s) throws UserResolutionException {
        return null;
    }

    @Override
    public Iterable<String> findGroupNamesByPrefix(String s, int i, int i1) {
        return null;
    }
}
