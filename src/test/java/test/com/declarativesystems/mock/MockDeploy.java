package test.com.declarativesystems.mock;

import com.declarativesystems.pejava.codemanager.Deploy;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class MockDeploy implements Deploy {

    @Override
    public String deployCode(String puppetMasterFqdn, String token, String caCert, String[] environment) throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, CertificateException
    {
        return "{\"mockdeploy\": \"ok\"}";
    }

    @Override
    public String deployCode(String puppetMasterFqdn, String token, String caCert, String[] environment, boolean wait) throws IOException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException, CertificateException
    {
        return "{\"mockdeploy\": \"complete\"}";
    }
}
