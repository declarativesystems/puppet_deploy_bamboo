package test.com.declarativesystems.mock;

import com.atlassian.upm.api.license.PluginLicenseManager;
import com.atlassian.upm.api.license.entity.PluginLicense;
import com.atlassian.upm.api.util.Option;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import org.mockito.Mockito;

public class MockPluginLicenseManagerImpl extends Mockito implements
        PluginLicenseManager, MockPluginLicenseManager {


    private boolean licensed = false;

    @Override
    public void setLicensed(boolean licensed) {
        this.licensed = licensed;
    }

    @Override
    public Option<PluginLicense> getLicense() {
        // FIXME... why the heck doesn't this work...
        Option<PluginLicense> option = mock(Option.class);
        doReturn(licensed).when(option).isDefined();
        return option;
    }

    @Override
    public boolean isUserInLicenseRole(String s) {
        return false;
    }

    @Override
    public Option<Integer> getCurrentUserCountInLicenseRole() {
        return null;
    }

    @Override
    public String getPluginKey() {
        return null;
    }
}
