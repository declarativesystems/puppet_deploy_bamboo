package test.com.declarativesystems.mock;

public interface MockPluginLicenseManager {
    void setLicensed(boolean licensed);
}
